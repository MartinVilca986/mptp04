import os

def menu():
    print('''
    a)_Registrar Productos.
    b)_Mostrar productos.
    c)_Mostrar productos en un intervalo.
    d)_Aumentar stock de productos con stock determinado.
    e)_Eliminar productos sin stock.
    f)_Fin del programa
    ''')
    opcionesValidas = ['a', 'b', 'c', 'd', 'e', 'f']
    opcionAux = input('Elija una de las opciones: ')
    while opcionAux not in opcionesValidas:
        opcionAux = input('Elija una de las opciones: ')
    return opcionAux

def cargarCodigo(diccAux):
    codigoAux = int(input('Ingrese el codigo del producto: '))
    while codigoAux in diccAux:
        print('Este codigo ya esta en uso.')
        codigoAux = int(input('Ingrese el codigo del producto: '))
    return codigoAux

def cargarDescripcion():
    descripcionAux = input('Ingrese la descripcion del producto: ')
    while descripcionAux == '':
        descripcionAux = input('Ingrese la descripcion del producto: ')
    return descripcionAux

def cargarPrecio():
    precioAux = float(input('Ingrese el precio del producto:'))
    while precioAux <= 0:
        print('el precio ingresado no es valido.')
        precioAux = float(input('Ingrese el precio del producto:'))
    return precioAux

def cargarStock():
    stockAux = int(input('Ingrese el stock del producto: '))
    while stockAux < 0:
        print('El stock ingresado no es valido.')
        stockAux = int(input('Ingrese el stock del producto: '))
    return stockAux

def cargarProductos(diccionarioAux1):
    os.system('cls')
    continValid = ['si', 'Si', 's', 'yes', 'Yes', 'y']
    continuarAux = 'si'
    while continuarAux in continValid:
        codigo = cargarCodigo(diccionarioAux1)
        descripcion = cargarDescripcion()
        precio = cargarPrecio()
        stock = cargarStock()
        diccionarioAux1[codigo] = [descripcion, precio, stock]
        print('El producto ha sido cargado.')
        continuarAux = input('Continuar cargando productos?: ')

def mostrarProductos(diccionarioAux2):
    os.system('cls')
    for clave, item in diccionarioAux2.items():
        print(clave, item)

def cargarHasta(desdeAux):
    hastaAux = int(input('Hasta que stock mostrar?: '))
    while hastaAux < desdeAux:
        hastaAux = int(input('Hasta que stock mostrar?: '))
    return hastaAux

def mostrarDesdeHasta(diccionarioAux3):
    os.system('cls')
    desde = int(input('Desde que stock mostrar?: '))
    hasta = cargarHasta(desde)
    for clave, valor in diccionarioAux3.items():
        if valor[2] >= desde and valor[2] <= hasta:
            print(clave, valor)

def aumentarStockProductos(diccionarioAux4):
    os.system('cls')
    stockParametro = int(input('A partir de que cantidad se va a aumentar el stock?: '))
    cantidadParaAgregar = int(input('Que cantidad de stock desea agregarle a los productos?: '))
    for clave, valor in diccionarioAux4.items():
        if valor[2] < stockParametro:
            diccionarioAux4[clave][2] += cantidadParaAgregar
            print(f'Se ha aumentado el stock del producto {diccionarioAux4[clave][0]}')

def tieneElementosSinStock(diccionarioAux5):
    sinStock = False
    claveAux = None
    for clave, valor in diccionarioAux5.items():
        if valor[2] == 0:
            sinStock = True
            claveAux = clave
            break
    return sinStock, claveAux
    
def eliminarProductosSinStock(diccionarioAux6):
    os.system('cls')
    elementosSinStock, clave = tieneElementosSinStock(diccionarioAux6)
    while elementosSinStock:
        del diccionarioAux6[clave]
        elementosSinStock, clave = tieneElementosSinStock(diccionarioAux6)

def finDelPrograma():
    print('~ .' * 15)
    print('Fin del programa.')
    print('~ .' * 15)

#principal
os.system('cls')
diccionarioProductos = {}
opcion = '1'
while opcion != 'f':
    opcion = menu()
    if opcion == 'a':
        cargarProductos(diccionarioProductos)
    elif opcion == 'b' and diccionarioProductos != {}:
        mostrarProductos(diccionarioProductos)
    elif opcion == 'c' and diccionarioProductos != {}:
        mostrarDesdeHasta(diccionarioProductos)
    elif opcion == 'd' and diccionarioProductos != {}:
        aumentarStockProductos(diccionarioProductos)
    elif opcion == 'e' and diccionarioProductos != {}:
        eliminarProductosSinStock(diccionarioProductos)
    if diccionarioProductos == {}:
        print('Tiene que cargar los productos (opcion "a")')
    if opcion != 'f':
        input('Precione enter para continuar.')

os.system('cls')
finDelPrograma()