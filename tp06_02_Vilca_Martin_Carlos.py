from io import open
import os

def menu():
    print('''
    a)_Leer lista de vehiculos en el fichero.
    b)_Buscar un vehiculo por dominio.
    c)_Buscar un vehiculo por marca.
    d)_Guardar vehiculos de una sola marca en un nuevo fichero.
    ''')
    opcionesValidas = ['a', 'b', 'c', 'd']
    opcionAux = input('Elija una de las opciones: ')
    while opcionAux not in opcionesValidas:
        opcionAux = input('Elija una de las opciones: ')
    return opcionAux

#lectura del fichero

def separarYCargar(dicc, vehiculoAux):
    datosVehiculo = vehiculoAux.split(';')
    listaAux = []
    for item in range(1, 8):
        listaAux.append(datosVehiculo[item])
    dicc[datosVehiculo[0]] = listaAux

def llenarDiccionario(listaAux):
    diccionarioAux = {}
    for item in listaAux:
        separarYCargar(diccionarioAux, item)
    input('Datos guardados en el diccionario. Precione enter.')
    return diccionarioAux

def leerFicheroVehiculos():
    vehiculos = open('vehiculos.txt', 'r')
    listadoVehiculos = vehiculos.readlines()
    vehiculos.close()
    diccionarioVehiculos = llenarDiccionario(listadoVehiculos)
    return diccionarioVehiculos

#buscar por dominio

def ordenamientoDeShell(listaAux, parametroAux):
    contadorSublistas = len(listaAux)//2
    while contadorSublistas > 0:
        for posicionInicio in range(contadorSublistas):
            brechaOrdenamientoPorInsercion(listaAux,posicionInicio,contadorSublistas, parametroAux)
        contadorSublistas = contadorSublistas // 2
    input('Datos ordenados correctamente. Presione enter.')

def brechaOrdenamientoPorInsercion(unaLista,inicio,brecha, parametroAux):
    for i in range(inicio+brecha,len(unaLista),brecha):
        valorActual = unaLista[i]
        posicion = i
        while posicion>=brecha and unaLista[posicion-brecha][parametroAux]>valorActual[parametroAux]:
            unaLista[posicion]=unaLista[posicion-brecha]
            posicion = posicion-brecha
        unaLista[posicion]=valorActual

def cargarDominio():
    dominioAux = input('Ingrese el dominio buscado (en mayuscula, sin espacio ni interrupciones): ')
    while not (len(dominioAux) == 6 or len(dominioAux) == 7) or dominioAux.isalnum() == False:
        if not(len(dominioAux) == 6 or len(dominioAux) == 7):
            print('La longitud del dominio ingresado no es valida.')
        elif len(dominioAux) == 7:
            if dominioAux[:1].isalpha() == False or dominioAux[-1:-2].isalpha() == False or dominioAux[2:8].isnumeric() == False:
                print('La estructura del dominio ingresado no es correcta.\nXX000XX (X=letra, 0=numero)')
        elif len(dominioAux) == 6:
            if dominioAux[:3].isalpha() == False or dominioAux[4:].isnumeric() == False:
                print('La estructura del dominio ingresado no es correcta.\nXXX000 (X=letra, 0=numero)')
        dominioAux = input('Ingrese el dominio buscado (en mayuscula, sin espacio ni interrupciones): ')
    return dominioAux

def busquedaBinaria(listaAux, dominioAux):
    enconrtado = False
    tope = len(listaAux) - 1
    piso = 0
    while listaAux[piso] <= listaAux[tope]:
        medio = (tope + piso) // 2
        if listaAux[medio][0] == dominioAux:
            print(listaAux[medio])
            enconrtado = True
            break
        elif dominioAux > listaAux[medio][0]:
            piso = medio + 1
            print(listaAux[piso], listaAux[medio])
        else:
            tope = medio - 1
            print(listaAux[piso], listaAux[tope])
    if enconrtado == False:
        print(f'No se ha encontrado ningun vehiculo con el dominio {dominioAux}, en el inventario.')

def buscarPorDominio(diccionarioAux):
    listaNueva = []
    for clave in diccionarioAux.keys():
        listaNueva.append([clave] + diccionarioAux[clave])
    dominioBuscado = cargarDominio()
    ordenamientoDeShell(listaNueva, 0)
    busquedaBinaria(listaNueva, dominioBuscado)

#buscar por marca

def convertirALista(diccionarioAux):
    listaNueva = []
    for clave in diccionarioAux.keys():
        listaNueva.append([clave] + diccionarioAux[clave])
    return listaNueva

def cargarMarca():
    marcaAux = input('Ingrese la marca buscada: ')
    while marcaAux.isalpha() != True:
        print('La marca ingresada no es valida.')
        marcaAux = input('Ingrese la marca buscada: ')
    return marcaAux

def buscarPorMarca(diccionarioAux):
    datosVehiculos = convertirALista(diccionarioAux)
    marcaBuscada = cargarMarca()
    ordenamientoDeShell(datosVehiculos, 1)
    for item in datosVehiculos:
        if marcaBuscada.upper() in item[1] or marcaBuscada.lower() in item[1]:
            print(item)

#crear nuevo ficheros

def convertirEnLista(diccionarioAux):
    nuevaLista = []
    for item in diccionarioAux:
        nuevaLista.append([item] + diccionarioAux[item])
    return nuevaLista

def marcasValidas(listaAux):
    marcasAux = []
    for item in listaAux:
        if item[1] not in marcasAux:
            marcasAux.append(item[1])
    return marcasAux

def cargarMarcaParaFichero(marcasValidasAux):
    marcaAux = input('Ingrese la marca de los automoviles: ')
    while marcaAux.isalpha() != True or marcaAux not in marcasValidasAux:
        print('La marca ingresada no es valida.')
        marcaAux = input('Ingrese la marca de los automoviles: ')
    return marcaAux

def cargarNuevoFichero(marcaAux, listaAux):
    nuevoFichero = open(marcaAux + '.txt', 'w')
    listaParaAgregar = []
    for item in listaAux:
        if item[1] == marcaAux:
            listaParaAgregar.append(';'.join(item))
    nuevoFichero.write(''.join(listaParaAgregar))
    nuevoFichero.close()
    print(f'El fichero {marcaAux}.txt ha sido creado con exito')

def crearNuevoFichero(diccAux):
    listaDatos = convertirEnLista(diccAux)
    marca = cargarMarcaParaFichero(marcasValidas(listaDatos))
    cargarNuevoFichero(marca, listaDatos)

#Fin del programa

def finDelPrograma():
    print('*' * 25)
    print('Fin del Programa')
    print('*' * 25)

#principal
continuarValidos = ['s', 'si', 'y', 'yes']
continuar = 's'
diccionarioDatosVehiculos = None
while continuar in continuarValidos:
    opcion = menu()
    if opcion == 'a':
        diccionarioDatosVehiculos = leerFicheroVehiculos()
    elif opcion == 'b':
        if diccionarioDatosVehiculos != None:
            buscarPorDominio(diccionarioDatosVehiculos)
    elif opcion == 'c':
        if diccionarioDatosVehiculos != None:
            buscarPorMarca(diccionarioDatosVehiculos)
    elif opcion == 'd':
        if diccionarioDatosVehiculos != None:
            crearNuevoFichero(diccionarioDatosVehiculos)
    if diccionarioDatosVehiculos == None:
        print('Debe cargar los datos del fichero para poder trabajar.')
    continuar = input('Desea continuar en el programa?: ')
    os.system('cls')
